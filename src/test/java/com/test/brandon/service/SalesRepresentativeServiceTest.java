package com.test.brandon.service;

import com.test.brandon.model.SalesRepresentative;
import com.test.brandon.repository.SalesRepresentativeRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SalesRepresentativeServiceTest {

    @Mock
    private SalesRepresentativeService salesRepresentativeService;

    @Mock
    private SalesRepresentativeRepository salesRepresentativeRepository;

    private SalesRepresentative salesRepresentative;
    private SalesRepresentative salesRepresentativeResult;
    private List<SalesRepresentative> salesRepresentativeList;
    @Before
    public void setUp() {
        salesRepresentative = new SalesRepresentative();
        salesRepresentativeResult = new SalesRepresentative();
        salesRepresentative.setName("salesRepresentative");
        salesRepresentative.setName("salesRepresentative");
        salesRepresentativeResult.setId(1);
        salesRepresentativeResult.setName("salesRepresentative");
        salesRepresentativeList = new ArrayList<>();
    }

    @Test
    public void getSalesRepresentatives() {
        Mockito.when(salesRepresentativeRepository.findAll()).thenReturn(salesRepresentativeList);
        Assert.assertNotNull(salesRepresentativeService.getSalesRepresentatives());
    }

    @Test
    public void saveSalesRepresentative() {
        Mockito.when(salesRepresentativeRepository.save(salesRepresentative)).thenReturn(salesRepresentativeResult);
        Assert.assertNotNull(salesRepresentativeService.saveSalesRepresentative(salesRepresentative.getDTO()));
    }
}