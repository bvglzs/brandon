package com.test.brandon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrandonApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrandonApplication.class, args);
	}

}
