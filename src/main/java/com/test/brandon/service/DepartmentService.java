package com.test.brandon.service;

import com.test.brandon.dto.CountryDto;
import com.test.brandon.dto.DepartmentDto;
import com.test.brandon.model.Country;
import com.test.brandon.model.Department;
import com.test.brandon.repository.DepartmentRepository;
import com.test.brandon.utils.ResponseConstants;
import com.test.brandon.utils.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public List<DepartmentDto> getDepartments(Integer countryId){
        List<Department> departmentList = departmentRepository.findByCountryId(countryId);
        return departmentList.stream().map(Department::getDTO).collect(Collectors.toList());
    }

    public List<DepartmentDto> getAllDepartments(){
        List<Department> departmentList = (List<Department>) departmentRepository.findAll();
        return departmentList.stream().map(Department::getDTO).collect(Collectors.toList());
    }

    public boolean saveDepartment(DepartmentDto departmentDto) {
        Department department = new Department(departmentDto);
        return departmentRepository.save(department).getId() != null;
    }
}
