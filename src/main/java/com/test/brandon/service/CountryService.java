package com.test.brandon.service;

import com.test.brandon.dto.CountryDto;
import com.test.brandon.model.Country;
import com.test.brandon.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CountryService {

    @Autowired
    private CountryRepository countryRepository;

    public List<CountryDto> getCountries(){
        List<Country> countryList = (List<Country>) countryRepository.findAll();
        return countryList.stream().map(Country::getDTO).collect(Collectors.toList());
    }

    public boolean saveCountry(CountryDto countryDto) {
        Country country = new Country(countryDto);
        return countryRepository.save(country).getId() != null;
    }
}
