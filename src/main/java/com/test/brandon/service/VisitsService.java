package com.test.brandon.service;

import com.test.brandon.dto.ChartDataDto;
import com.test.brandon.dto.CityDto;
import com.test.brandon.dto.VisitsDto;
import com.test.brandon.model.City;
import com.test.brandon.model.Client;
import com.test.brandon.model.Visits;
import com.test.brandon.repository.VisitsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class VisitsService {

    @Autowired
    private VisitsRepository visitsRepository;

    @Autowired
    private CityService cityService;

    public List<VisitsDto> getAllVisitsByClient(Integer clientId){
        List<Visits> visitsList = visitsRepository.findByClientId(clientId);
        return visitsList.stream().map(Visits::getDTO).collect(Collectors.toList());
    }

    public boolean saveVisits(VisitsDto visitsDto){
        Visits visit = new Visits(visitsDto);
        return visitsRepository.save(visit).getId() != null;
    }

    public List<ChartDataDto> getDataReport(){
        List<CityDto> cities = cityService.getAllCities();
        List<ChartDataDto> chartDataDtoList = new ArrayList<>();
        for (CityDto city :
                cities) {
            ChartDataDto chartDataDto = new ChartDataDto();
            long sumVisits = visitsRepository.countByClientCityId(city.getId());
            chartDataDto.setCityName(city.getName());
            chartDataDto.setVisits(sumVisits);
            chartDataDtoList.add(chartDataDto);
        }
        return chartDataDtoList;
    }

    public boolean deleteVisit(Integer visitsId) throws Exception {
        Optional<Visits> visit = visitsRepository.findById(visitsId);
        if(visit.isPresent()){
            try {
                visitsRepository.delete(visit.get());
                return true;
            }catch (Exception e){
                throw  new Exception("Error deleting visit");
            }
        }else{
            throw  new Exception("VisitId doesn't exist");
        }
    }
}
