package com.test.brandon.service;

import com.test.brandon.dto.CityDto;
import com.test.brandon.model.City;
import com.test.brandon.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CityService {

    @Autowired
    private CityRepository cityRepository;

    public List<CityDto> getCitiesByDepartmentId(Integer departmentId){
        List<City> cityDtoList = cityRepository.findByDepartmentId(departmentId);
        return cityDtoList.stream().map(City::getDTO).collect(Collectors.toList());
    }

    public List<CityDto> getAllCities(){
        List<City> cityDtoList = (List<City>) cityRepository.findAll();
        return cityDtoList.stream().map(City::getDTO).collect(Collectors.toList());
    }

    public boolean saveCity(CityDto cityDto) {
        City city = new City(cityDto);
        return cityRepository.save(city).getId() != null;
    }
}
