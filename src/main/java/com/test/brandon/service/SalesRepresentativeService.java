package com.test.brandon.service;

import com.test.brandon.dto.SalesRepresentativeDto;
import com.test.brandon.model.SalesRepresentative;
import com.test.brandon.repository.SalesRepresentativeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SalesRepresentativeService {

    @Autowired
    private SalesRepresentativeRepository salesRepresentativeRepository;

    public List<SalesRepresentativeDto> getSalesRepresentatives(){
        List<SalesRepresentative> salesRepresentatives = (List<SalesRepresentative>) salesRepresentativeRepository.findAll();
        return salesRepresentatives.stream().map(SalesRepresentative::getDTO).collect(Collectors.toList());
    }

    public boolean saveSalesRepresentative(SalesRepresentativeDto salesRepresentativeDto){
        SalesRepresentative salesRepresentative = new SalesRepresentative(salesRepresentativeDto);
        return salesRepresentativeRepository.save(salesRepresentative).getId() != null;
    }
}
