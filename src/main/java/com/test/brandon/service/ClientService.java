package com.test.brandon.service;

import com.test.brandon.dto.ClientDto;
import com.test.brandon.dto.VisitsDto;
import com.test.brandon.model.Client;
import com.test.brandon.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private VisitsService visitsService;

    public List<ClientDto> getClients(){
        List<Client> clientList = (List<Client>)  clientRepository.findAll();
        return clientList.stream().map(client -> {
            byte[] decodedBytes = Base64.getDecoder().decode(client.getNit());
            client.setNit(new String(decodedBytes));
            return client;
        }).collect(Collectors.toList()).stream().map(Client::getDTO).collect(Collectors.toList());
    }

    public boolean saveClient(ClientDto clientDto){
        Client client = new Client(clientDto);
        return clientRepository.save(client).getId() != null;
    }

    public boolean deleteClient(Integer clientId) throws Exception {
        Optional<Client> client = clientRepository.findById(clientId);
        if(client.isPresent()){
            try {
                visitsService.getAllVisitsByClient(clientId).stream().forEach(visit -> {
                    try {
                        visitsService.deleteVisit(visit.getId());
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                });
                clientRepository.delete(client.get());
                return true;
            }catch (Exception e){
                throw  new Exception("Error deleting client");
            }
        }else{
            throw  new Exception("ClientId doesn't exist");
        }
    }
}
