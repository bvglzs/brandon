package com.test.brandon.utils;

/**
 * Class for handling generic service response.
 *
 * @author Habib De León - Cidenet
 * @version 1.00, 11 jul 2018
 */
public class ResponseObject<T> {

    private String status;

    private String message;

    private T body;

    public ResponseObject(T body) {
        this.body = body;
        this.status = ResponseConstants.SUCCESS_STATUS;
        this.message = ResponseConstants.SUCCESS_MESSAGE;
    }

    public ResponseObject(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public ResponseObject(T body, String status, String message) {
        this.body = body;
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }
}