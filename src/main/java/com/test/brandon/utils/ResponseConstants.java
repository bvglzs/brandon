package com.test.brandon.utils;

/**
 * This class contains the constants used in message responses
 *
 * @author Pablo Muñoz - Cidenet
 * @version 1.00, 06 Jul 2018
 */
public class ResponseConstants {

    public static final String SUCCESS_STATUS = "SUCCESS";
    public static final String SUCCESS_MESSAGE = "Operation finished successfully";

    public static final String ERROR_STATUS = "ERROR";
    public static final String ERROR_MESSAGE = "An error has occurred";

    public static final String NOT_FOUND_STATUS = "NOT FOUND";
    public static final String NOT_FOUND_MESSAGE = "Resource not found";

    public static final String ID_MANDATORY_MESSAGE = "Resource Id is mandatory.";
    public static final String ALL_FIELDS_MANDATORY = "All fields are mandatory.";
    public static final String CANNOT_UDDATE_ID = "The id can't be updated";
}
