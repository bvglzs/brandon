package com.test.brandon.utils;

import com.test.brandon.dto.DTO;
import com.test.brandon.model.BasicEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class Utilities extends DTO {

    public static <T extends DTO> List<T> convertEntityListToDTO(List<? extends BasicEntity<? extends T>> entities) {
        List<T> dtos = new ArrayList<>();
        for (BasicEntity<? extends T> entity : entities) {
            dtos.add(entity.getDTO());
        }
        return dtos;
    }
}
