package com.test.brandon.repository;

import com.test.brandon.model.Visits;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VisitsRepository extends CrudRepository<Visits, Integer> {
    List<Visits> findByClientId(Integer clientId);
    long countByClientCityId(Integer cityId);
}
