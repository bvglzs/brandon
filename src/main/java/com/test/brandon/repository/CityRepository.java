package com.test.brandon.repository;

import com.test.brandon.model.City;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends CrudRepository<City, Integer> {
    List<City> findByDepartmentId(Integer departmentId);
}
