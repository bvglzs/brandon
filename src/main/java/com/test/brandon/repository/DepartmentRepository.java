package com.test.brandon.repository;

import com.test.brandon.model.Department;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Integer> {
    List<Department> findByCountryId(Integer countryId);
}
