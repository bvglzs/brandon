package com.test.brandon.repository;

import com.test.brandon.model.SalesRepresentative;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalesRepresentativeRepository extends CrudRepository<SalesRepresentative, Integer> {
}
