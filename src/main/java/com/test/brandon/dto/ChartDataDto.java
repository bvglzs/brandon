package com.test.brandon.dto;

public class ChartDataDto extends DTO {

    private String cityName;
    private long visits;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public long getVisits() {
        return visits;
    }

    public void setVisits(long visits) {
        this.visits = visits;
    }
}
