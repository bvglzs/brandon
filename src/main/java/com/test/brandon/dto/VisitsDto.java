package com.test.brandon.dto;

import java.util.Date;

public class VisitsDto extends DTO {

    private Integer id;
    private Date visitDate;
    private SalesRepresentativeDto salesRepresentative;
    private ClientDto client;
    private Double net;
    private Double visit_total;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public SalesRepresentativeDto getSalesRepresentative() {
        return salesRepresentative;
    }

    public void setSalesRepresentative(SalesRepresentativeDto salesRepresentative) {
        this.salesRepresentative = salesRepresentative;
    }

    public ClientDto getClient() {
        return client;
    }

    public void setClient(ClientDto client) {
        this.client = client;
    }

    public Double getNet() {
        return net;
    }

    public void setNet(Double net) {
        this.net = net;
    }

    public Double getVisit_total() {
        return visit_total;
    }

    public void setVisit_total(Double visit_total) {
        this.visit_total = visit_total;
    }
}
