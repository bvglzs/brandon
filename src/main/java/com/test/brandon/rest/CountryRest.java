package com.test.brandon.rest;

import com.test.brandon.dto.CityDto;
import com.test.brandon.dto.CountryDto;
import com.test.brandon.model.Country;
import com.test.brandon.service.CountryService;
import com.test.brandon.utils.ResponseConstants;
import com.test.brandon.utils.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/country")
public class CountryRest {

    @Autowired
    private CountryService countryService;

    @GetMapping("")
    public ResponseObject<List<CountryDto>> getCountries() {
        return new ResponseObject<>(countryService.getCountries(), ResponseConstants.SUCCESS_STATUS,
                ResponseConstants.SUCCESS_MESSAGE);
    }

    @PostMapping("/saveCountry")
    public ResponseObject<List<CountryDto>> saveCountry(@RequestBody CountryDto countryDto) {
        if(countryService.saveCountry(countryDto)){
            return new ResponseObject<>(ResponseConstants.SUCCESS_STATUS,
                    ResponseConstants.SUCCESS_MESSAGE);
        }else{
            return new ResponseObject<>(ResponseConstants.ERROR_STATUS,
                    ResponseConstants.ERROR_MESSAGE);
        }
    }
}
