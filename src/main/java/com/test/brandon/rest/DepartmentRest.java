package com.test.brandon.rest;

import com.test.brandon.dto.CountryDto;
import com.test.brandon.dto.DepartmentDto;
import com.test.brandon.model.Country;
import com.test.brandon.model.Department;
import com.test.brandon.service.DepartmentService;
import com.test.brandon.utils.ResponseConstants;
import com.test.brandon.utils.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/department")
public class DepartmentRest {

    @Autowired
    private DepartmentService departmentService;

    @GetMapping("/{countryId}")
    public ResponseObject<List<DepartmentDto>> getCountries(@PathVariable("countryId") Integer countryId) {
        return new ResponseObject<>(departmentService.getDepartments(countryId), ResponseConstants.SUCCESS_STATUS,
                ResponseConstants.SUCCESS_MESSAGE);
    }

    @GetMapping("")
    public ResponseObject<List<DepartmentDto>> getAllDepartments() {
        return new ResponseObject<>(departmentService.getAllDepartments(), ResponseConstants.SUCCESS_STATUS,
                ResponseConstants.SUCCESS_MESSAGE);
    }

    @PostMapping("/saveDepartment")
    public ResponseObject<Void> saveDepartment(@RequestBody DepartmentDto departmentDto) {
        if(departmentService.saveDepartment(departmentDto)){
            return new ResponseObject<>(ResponseConstants.SUCCESS_STATUS,
                    ResponseConstants.SUCCESS_MESSAGE);
        }else{
            return new ResponseObject<>(ResponseConstants.ERROR_STATUS,
                    ResponseConstants.ERROR_MESSAGE);
        }
    }
}
