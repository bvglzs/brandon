package com.test.brandon.rest;

import com.test.brandon.dto.ClientDto;
import com.test.brandon.model.Client;
import com.test.brandon.service.ClientService;
import com.test.brandon.utils.ResponseConstants;
import com.test.brandon.utils.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientRest {

    @Autowired
    private ClientService clientService;

    @GetMapping("")
    public ResponseObject<List<ClientDto>> getClients(){
        return new ResponseObject<>(clientService.getClients(), ResponseConstants.SUCCESS_STATUS,
                ResponseConstants.SUCCESS_MESSAGE);
    }

    @GetMapping("/{clientId}")
    public ResponseObject<List<ClientDto>> deleteclient(@PathVariable("clientId")  Integer clienteId) throws Exception {
        if(clientService.deleteClient(clienteId)){
            return new ResponseObject<>(ResponseConstants.SUCCESS_STATUS,
                    ResponseConstants.SUCCESS_MESSAGE);
        }
        return new ResponseObject<>(ResponseConstants.ERROR_STATUS,
                ResponseConstants.ERROR_MESSAGE);
    }

    @PostMapping("/saveClient")
    public ResponseObject<List<ClientDto>> saveClient(@RequestBody ClientDto clientDto){
        if(clientService.saveClient(clientDto)){
            return new ResponseObject<>(ResponseConstants.SUCCESS_STATUS,
                    ResponseConstants.SUCCESS_MESSAGE);
        }else{
            return new ResponseObject<>(ResponseConstants.ERROR_STATUS,
                    ResponseConstants.ERROR_MESSAGE);
        }
    }
}
