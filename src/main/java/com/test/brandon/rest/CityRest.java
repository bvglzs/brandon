package com.test.brandon.rest;

import com.test.brandon.dto.CityDto;
import com.test.brandon.dto.DepartmentDto;
import com.test.brandon.model.City;
import com.test.brandon.model.Department;
import com.test.brandon.service.CityService;
import com.test.brandon.utils.ResponseConstants;
import com.test.brandon.utils.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/city")
public class CityRest {

    @Autowired
    private CityService cityService;

    @GetMapping("/{departmentId}")
    public ResponseObject<List<CityDto>> getCities(@PathVariable("departmentId") Integer departmentId) {
        return new ResponseObject<>(cityService.getCitiesByDepartmentId(departmentId), ResponseConstants.SUCCESS_STATUS,
                ResponseConstants.SUCCESS_MESSAGE);
    }

    @GetMapping("")
    public ResponseObject<List<CityDto>> getAllCities() {
        return new ResponseObject<>(cityService.getAllCities(), ResponseConstants.SUCCESS_STATUS,
                ResponseConstants.SUCCESS_MESSAGE);
    }

    @PostMapping("/saveCity")
    public ResponseObject<Void> saveCity(@RequestBody CityDto cityDto) {
        if(cityService.saveCity(cityDto)){
            return new ResponseObject<>(ResponseConstants.SUCCESS_STATUS,
                    ResponseConstants.SUCCESS_MESSAGE);
        }else{
            return new ResponseObject<>(ResponseConstants.ERROR_STATUS,
                    ResponseConstants.ERROR_MESSAGE);
        }
    }
}
