package com.test.brandon.rest;

import com.test.brandon.dto.SalesRepresentativeDto;
import com.test.brandon.model.SalesRepresentative;
import com.test.brandon.service.SalesRepresentativeService;
import com.test.brandon.utils.ResponseConstants;
import com.test.brandon.utils.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/salesRepresentative")
public class SalesRepresentativeRest {

    @Autowired
    private SalesRepresentativeService salesRepresentativeService;

    @GetMapping("")
    public ResponseObject<List<SalesRepresentativeDto>> getSalesRepresentatives(){
        return new ResponseObject<>(salesRepresentativeService.getSalesRepresentatives(), ResponseConstants.SUCCESS_STATUS,
                ResponseConstants.SUCCESS_MESSAGE);
    }

    @PostMapping("/saveSalesRepresentative")
    public ResponseObject<Void> saveSalesRepresentative(@RequestBody SalesRepresentativeDto salesRepresentativeDto){
        if(salesRepresentativeService.saveSalesRepresentative(salesRepresentativeDto)){
            return new ResponseObject<>(ResponseConstants.SUCCESS_STATUS,
                    ResponseConstants.SUCCESS_MESSAGE);
        }else{
            return new ResponseObject<>(ResponseConstants.ERROR_STATUS,
                    ResponseConstants.ERROR_MESSAGE);
        }
    }
}
