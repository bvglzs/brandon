package com.test.brandon.rest;

import com.test.brandon.dto.ChartDataDto;
import com.test.brandon.dto.VisitsDto;
import com.test.brandon.service.VisitsService;
import com.test.brandon.utils.ResponseConstants;
import com.test.brandon.utils.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("visits")
public class VisitsRest {

    @Autowired
    private VisitsService visitsService;

    @GetMapping("/{clientId}")
    public ResponseObject<List<VisitsDto>> getAllVisitsByClient(@PathVariable("clientId") Integer clientId){
        return new ResponseObject<>(visitsService.getAllVisitsByClient(clientId), ResponseConstants.SUCCESS_STATUS,
                ResponseConstants.SUCCESS_MESSAGE);
    }

    @GetMapping("/getDataReport")
    public ResponseObject<List<ChartDataDto>> getDataReport(){
        return new ResponseObject<>(visitsService.getDataReport(), ResponseConstants.SUCCESS_STATUS,
                ResponseConstants.SUCCESS_MESSAGE);
    }

    @PostMapping("/saveVisits")
    public ResponseObject<List<VisitsDto>> saveVisits(@RequestBody VisitsDto visitsDto){
        if(visitsService.saveVisits(visitsDto)){
            return new ResponseObject<>(ResponseConstants.SUCCESS_STATUS,
                    ResponseConstants.SUCCESS_MESSAGE);
        }else{
            return new ResponseObject<>(ResponseConstants.ERROR_STATUS,
                    ResponseConstants.ERROR_MESSAGE);
        }
    }
}
