package com.test.brandon.model;

import com.test.brandon.dto.DTO;
import org.springframework.beans.BeanUtils;

public abstract class BasicEntity<T extends DTO> {
    public abstract T getDTO();

    public BasicEntity() {

    }

    public BasicEntity(T dto) {
        BeanUtils.copyProperties(dto, this);
    }
}