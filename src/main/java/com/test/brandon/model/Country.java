package com.test.brandon.model;

import com.test.brandon.dto.CountryDto;
import org.springframework.beans.BeanUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "country")
public class Country extends BasicEntity<CountryDto>{

    @Id
    @SequenceGenerator(name="country_id_seq",
            sequenceName="country_id_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="country_id_seq")
    private Integer id;

    @Column(name = "name")
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country() {
        super();
    }

    public Country(CountryDto countryDto) {
        super(countryDto);
    }

    @Override
    public CountryDto getDTO() {
        CountryDto countryDto = new CountryDto();
        BeanUtils.copyProperties(this, countryDto);
        return countryDto;
    }
}
