package com.test.brandon.model;

import com.test.brandon.dto.VisitsDto;
import org.springframework.beans.BeanUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Base64;
import java.util.Date;

@Entity
@Table(name = "visits")
public class Visits extends BasicEntity<VisitsDto> {

    @Id
    @SequenceGenerator(name="visits_id_seq",
            sequenceName="visits_id_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="visits_id_seq")
    private Integer id;

    @Column(name = "visit_date")
    private Date visitDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sales_representative_id")
    private SalesRepresentative salesRepresentative;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    private Client client;

    @Column(name = "net")
    private Double net;

    @Column(name = "visit_total")
    private Double visit_total;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public SalesRepresentative getSalesRepresentative() {
        return salesRepresentative;
    }

    public void setSalesRepresentative(SalesRepresentative salesRepresentative) {
        this.salesRepresentative = salesRepresentative;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Double getNet() {
        return net;
    }

    public void setNet(Double net) {
        this.net = net;
    }

    public Double getVisit_total() {
        return visit_total;
    }

    public void setVisit_total(Double visit_total) {
        this.visit_total = visit_total;
    }

    public Visits(){
        super();
    }

    public Visits(VisitsDto visitsDto) {
        super(visitsDto);
        Client client = new Client(visitsDto.getClient());
        this.setClient(client);
        SalesRepresentative salesRepresentative = new SalesRepresentative(visitsDto.getSalesRepresentative());
        this.setSalesRepresentative(salesRepresentative);
    }

    @Override
    public VisitsDto getDTO() {
        VisitsDto visitsDto = new VisitsDto();
        BeanUtils.copyProperties(this,visitsDto);
        if(this.getClient() != null){
            visitsDto.setClient(this.getClient().getDTO());
        }
        if(this.getSalesRepresentative() != null){
            visitsDto.setSalesRepresentative(this.getSalesRepresentative().getDTO());
        }
        return visitsDto;
    }
}
