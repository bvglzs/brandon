package com.test.brandon.model;

import com.test.brandon.dto.ClientDto;
import org.springframework.beans.BeanUtils;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Base64;

@Entity
@Table(name = "clients")
public class Client extends BasicEntity<ClientDto> {

    @Id
    @SequenceGenerator(name="clients_id_seq",
            sequenceName="clients_id_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="clients_id_seq")
    private Integer id;

    @Column(name = "nit")
    private String nit;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id")
    private City city;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    private Department department;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    private Country country;

    @Column(name = "credit_limit")
    private Double creditLimit;

    @Column(name = "available_credit")
    private Double availableCredit;

    @Column(name = "visit_percentage")
    private Double visitPercentage;

    public Client(){
        super();
    }

    public Client(ClientDto clientDto) {
        super(clientDto);
        City city = new City(clientDto.getCityDto());
        Department department = new Department(clientDto.getDepartmentDto());
        Country country = new Country(clientDto.getCountryDto());
        this.setCity(city);
        this.setDepartment(department);
        this.setCountry(country);
        this.setNit(Base64.getEncoder().encodeToString(this.getNit().getBytes()));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Double getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(Double creditLimit) {
        this.creditLimit = creditLimit;
    }

    public Double getAvailableCredit() {
        return availableCredit;
    }

    public void setAvailableCredit(Double availableCredit) {
        this.availableCredit = availableCredit;
    }

    public Double getVisitPercentage() {
        return visitPercentage;
    }

    public void setVisitPercentage(Double visitPercentage) {
        this.visitPercentage = visitPercentage;
    }

    @Override
    public ClientDto getDTO() {
        ClientDto clientDto = new ClientDto();
        BeanUtils.copyProperties(this, clientDto);
        if(this.getCity() != null){
            clientDto.setCityDto(this.getCity().getDTO());
        }
        if(this.getDepartment() != null){
            clientDto.setDepartmentDto(this.getDepartment().getDTO());
        }
        if(this.getCountry() != null){
            clientDto.setCountryDto(this.getCountry().getDTO());
        }
        return clientDto;
    }
}
