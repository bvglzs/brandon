package com.test.brandon.model;

import com.test.brandon.dto.CityDto;
import org.springframework.beans.BeanUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "city")
public class City extends BasicEntity<CityDto>{

    @Id
    @SequenceGenerator(name="city_id_seq",
            sequenceName="city_id_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="city_id_seq")
    private Integer id;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    private Department department;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public City() {
        super();
    }

    public City(CityDto cityDto) {
        super(cityDto);
        Department department = new Department(cityDto.getDepartmentDto());
        this.setDepartment(department);
    }

    @Override
    public CityDto getDTO() {
        CityDto cityDto = new CityDto();
        BeanUtils.copyProperties(this, cityDto);
        if(this.getDepartment()!=null){
            cityDto.setDepartmentDto(this.getDepartment().getDTO());
        }
        return cityDto;
    }
}
