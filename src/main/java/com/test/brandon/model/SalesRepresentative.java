package com.test.brandon.model;

import com.test.brandon.dto.SalesRepresentativeDto;
import org.springframework.beans.BeanUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "sales_representative")
public class SalesRepresentative extends BasicEntity<SalesRepresentativeDto> {

    @Id
    @SequenceGenerator(name="sales_representative_id_seq",
            sequenceName="sales_representative_id_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="sales_representative_id_seq")
    private Integer id;

    @Column(name = "name")
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SalesRepresentative(){
        super();
    }

    public SalesRepresentative(SalesRepresentativeDto salesRepresentativeDto) {
        super(salesRepresentativeDto);
    }

    @Override
    public SalesRepresentativeDto getDTO() {
        SalesRepresentativeDto salesRepresentativeDto = new SalesRepresentativeDto();
        BeanUtils.copyProperties(this, salesRepresentativeDto);
        return salesRepresentativeDto;
    }
}
