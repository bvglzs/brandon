package com.test.brandon.model;

import com.test.brandon.dto.DepartmentDto;
import org.springframework.beans.BeanUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "department")
public class Department extends BasicEntity<DepartmentDto> {

    @Id
    @SequenceGenerator(name="department_id_seq",
            sequenceName="department_id_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="department_id_seq")
    private Integer id;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    private Country country;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Department(){
        super();
    }

    public Department(DepartmentDto departmentDto) {
        super(departmentDto);
        Country country = new Country(departmentDto.getCountryDto());
        this.setCountry(country);
    }

    @Override
    public DepartmentDto getDTO() {
        DepartmentDto departmentDto = new DepartmentDto();
        BeanUtils.copyProperties(this, departmentDto);
        if(this.getCountry() != null) {
            departmentDto.setCountryDto(this.getCountry().getDTO());
        }
        return departmentDto;
    }
}
