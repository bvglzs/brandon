CREATE SEQUENCE country_id_seq;
create table country
(
	id integer primary key DEFAULT nextval('country_id_seq'),
	name varchar(100) not null
);
ALTER SEQUENCE country_id_seq
OWNED BY country.id;
CREATE SEQUENCE department_id_seq;
create table department
(
	id integer PRIMARY KEY DEFAULT nextval('department_id_seq'),
	name varchar(100) not null,
	country_id integer not null
);
ALTER SEQUENCE department_id_seq
OWNED BY department.id;
ALTER TABLE department
  ADD  CONSTRAINT fk_department_country FOREIGN KEY (country_id) REFERENCES country(id);
CREATE SEQUENCE city_id_seq;
create table city
(
	id integer PRIMARY KEY DEFAULT nextval('city_id_seq'),
	name varchar(100) not null,
	department_id integer not null
);
ALTER TABLE city
  ADD  CONSTRAINT fk_city_department FOREIGN KEY (department_id) REFERENCES department(id);
ALTER SEQUENCE city_id_seq
OWNED BY city.id;
CREATE SEQUENCE sales_representative_id_seq;
create table sales_representative
(
	id integer primary key DEFAULT nextval('sales_representative_id_seq'),
	name varchar(100) not null
);
ALTER SEQUENCE sales_representative_id_seq
OWNED BY sales_representative.id;
CREATE SEQUENCE clients_id_seq;
create table clients
(
	id integer primary key DEFAULT nextval('clients_id_seq'),
	nit text not null,
	full_name varchar(255) not null,
	address varchar(255) not null,
	phone varchar(20) not null,
	city_id integer not null,
	department_id integer not null,
	country_id integer not null,
	credit_limit numeric not null,
	available_credit numeric not null,
	visit_percentage numeric not null
);
ALTER SEQUENCE clients_id_seq
OWNED BY clients.id;
ALTER TABLE clients
  ADD  CONSTRAINT fk_clients_city FOREIGN KEY (city_id) REFERENCES city(id);
ALTER TABLE clients
  ADD  CONSTRAINT fk_clients_department FOREIGN KEY (department_id) REFERENCES department(id);
ALTER TABLE clients
	ADD  CONSTRAINT fk_clients_country FOREIGN KEY (country_id) REFERENCES country(id);
CREATE SEQUENCE visits_id_seq;
create table visits
(
	id integer primary key DEFAULT nextval('visits_id_seq'),
	visit_date date not null,
	sales_representative_id integer not null,
	net numeric not null,
	visit_total numeric not null,
	client_id integer not null
);
ALTER SEQUENCE visits_id_seq
OWNED BY visits.id;
ALTER TABLE visits
  ADD  CONSTRAINT fk_visits_sales_representative FOREIGN KEY (sales_representative_id) REFERENCES sales_representative(id);
ALTER TABLE visits
  ADD  CONSTRAINT fk_visits_clients FOREIGN KEY (client_id) REFERENCES clients(id);